'use strict';

// Register `form` component, along with its associated controller and template
angular.
  module('noipa.form').
  component('formComponent', {
    templateUrl: 'form/form.template.html',
    controller: 
      function formController($scope,$http,FORM_CONFIG) {
        $scope.HEADER_CONFIG = FORM_CONFIG;
        $scope.esito = '';
        $scope.startProcess = function (){
          $http.get(FORM_CONFIG.serviceUrl).
          then(function(response) {
              $scope.esito = response.status;
          });
        }
      }
    
  });

  